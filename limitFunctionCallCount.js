const limitFunctionCallCount = (cb, n) => {
    if (!cb || !n) {return {};}
    let count = 0;
    return  (a,b) => {
        if (count < Number(n)){
            count ++;
            return cb(a,b);
        }
        else {return null;}
    };
};

module.exports = limitFunctionCallCount;
