const counterFactory = () => {
    let value =0;
    return {
        increment : function(){
            value++;
            return value;
        },
        decrement : function(){
            value--;
            return value;
        }
    };
};

module.exports = counterFactory;




