const cacheFunction = (cb) => {
    if(!cb) {return null;}
    let cache = {};
    return (...args) => {
        let n = args;
        if(!n.length){return null;}
        if (n in cache) {
            console.log('Fetching from cache');
            return cache[n];
        }
        else {
            console.log('Calculating result');
            let result = cb(...n);
            cache[n] = result;
            return result;
        }
    };
};

module.exports = cacheFunction;