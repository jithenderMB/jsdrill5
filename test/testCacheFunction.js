const cacheFunction = require("../cacheFunction");

const add = (a,b) => a + b;
const cachedAdd = cacheFunction(add);

console.log(cachedAdd(1,2)); 
console.log(cachedAdd(1,2)); 
console.log(cachedAdd(2,1)); 
console.log(cachedAdd(2,3)); 
console.log(cachedAdd(2,3)); 
