const counterFactory = require("../counterFactory");

const counter1 = counterFactory();
counter1.increment();
console.log(counter1.increment());
console.log(counter1.decrement());